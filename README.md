# HCS Code Challenge

Human Care Systems - Coding Challenge for full stack developer position.

## - ENV Dependencies

```
[Node.js 10.15.2](https://nodejs.org/en/)
```

## - Install backend dependencies

1) Open your terminal.
2) Run the lines listed below.

```
$ git clone https://gitlab.com/agcrov/hcs-code-challenge
$ cd hcs-code-challenge/server
$ npm install
```

## - Run backend server

1) Create an `.env` file inside the `./server` folder and insert the following variables:

```
PORT=8000
DB_URI=mongodb+srv://Agcrov:VXhr1D1llA174jfr@hcs-wqtdy.mongodb.net/test?retryWrites=true&w=majority
DB_KEY=a4880401-048f-4f5b-a477-fd3a45f1a5f4
SECRET=72amboGd8fePBGVDIc3RsSttckZTF13w
```

2) Run the following command in your terminal.

```
$ npm run start:dev
```

If you'd like to create an user you can make a HTTP POST call to this endpoint `http://localhost:8000/auth` with a JSON request body containing the following data:

* username
* password

Is open for you 😀

## - Install frontend dependencies

*If you already have the repo in your computer*

1) Open your terminal.
2) Run the lines listed below.

```
$ cd hcs-code-challenge/client
$ npm install
```

## - Run frontend server

Run the following command

```
$ npm start
```

🌎 Go to `http://localhost:3000` in your browser.

## How was my process

I started thinking about what had to be done, which technologies and how I was 
going to implement them, I had to do a little bit of research of the technologies
that I needed to implement.
The initial aproach was to do the backend first and then the frontend. 
I started with the entities models and then the `Authentication`
controller that handles all the auth request/validations, following to that 
I continued with the `Tasks` controller and enpoints without credentials checks.
Once I had a rough version of the backend I started to implement the frontend.
Just the basics at first, set up the state manager, the app router, connect 
React with Redux, reset css. When I had a running app I started creating 
the `Layout` component that wrapps the whole app. Next thing was to create 
reducer and actions for the auth and implement them into a `Login` component 
that is a redux-form. After I had the redux-form submiting and getting the token
and storing it into separeted cookies I moved on to the 'Home' component that is
the view where the user's tasks should be displayed. I implemented the redux 
reducer and actions for fetching the tasks.So I've reused some components from 
previous projects, like the table component and the modal wrapper component with
serveral modifications. After the tasks were fetched from the API I continue 
with the update/create (is the same one) redux-form component for `tasks`, 
the post/patch actions and add new cases to the task reducer.
When the modal, form and actions were working I added the `auth middleware` 
to all protected routes of the API. For the end I just added some functionalities 
of redirection on logout, invalid token. 


### Improvements: 
- Add unit tests
- Add e2e
- Split the AUTH logic into a separate service
- Improve security measures
- Split the git history into small commits
- Add CI with GitLab pipelines

# Author
[Agustin Crovetto](https://gitlab.com/agcrov)

