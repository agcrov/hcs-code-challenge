import PrivateRoute from './PrivateRoute';
import {verify} from '../../../redux/Auth/actions';
import {connect} from "react-redux";
import { bindActionCreators } from 'redux';

const mapStateToProps = state => ({
  auth: state.auth
});

const mapDispatchToProps = dispatch => bindActionCreators({
  verify
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PrivateRoute);
