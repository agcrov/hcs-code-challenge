import React from 'react';
import {Route, Redirect} from 'react-router-dom';
import {isUndefined} from 'lodash';

class PrivateRoute extends React.Component {
  componentDidMount() {
    this.props.verify();
  }
  render() {
    const { component: Component, ...rest } = this.props;
    return (
      <>
      {!isUndefined(rest.auth.authenticated) ? (
        <Route {...rest} render={props =>  rest.auth.authenticated ?
          <Component {...props} />
          : <Redirect to={{pathname: "/login", state: { from: props.location }}}/>
        }/>
      ): <div>Loading</div>}
      </>
    )
  }
}

export default PrivateRoute;