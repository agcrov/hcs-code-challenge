import React, {Component} from 'react';
import ReactTable from 'react-table';
import {withRouter} from "react-router-dom";
import 'react-table/react-table.css';
import Pagination from "./Pagination";
import './Table.scss';

export const TableTypes = {
  STRIPED_DEFAULT_TABLE: 'STRIPED_DEFAULT_TABLE',
  CUSTOM_ROW_TABLE: 'CUSTOM_ROW_TABLE',
};

const getStyles = (type) => {
    if (type === TableTypes.CUSTOM_ROW_TABLE) return 'custom-row-table';
    return 'default-table';
};

class Table extends Component {
  render() {
    const {type, columns, data, itemsPerPage, handleRedirectByRow, paginated} = this.props;
    const styles = getStyles(type);
    const striped = type !== TableTypes.CUSTOM_ROW_TABLE? '-striped -highlight' : '';
    return (
      <div className={styles}>
        <ReactTable
          PaginationComponent={Pagination}
          showPagination={paginated}
          resizable={true}
          filterable={false}
          sortable={false}
          data={data}
          columns={columns}
          pageSize={itemsPerPage || data.length}
          className={striped}
          getTrProps={(state, rowInfo) => ({
            onClick: () => {
              return handleRedirectByRow && handleRedirectByRow(rowInfo.original._id)
            }
          })}
        />
      </div>
    );
  }
}

export default withRouter(Table);
