import React from "react";
import PropTypes from "prop-types";
import './Pagination.css';
import simpleLeftChevron from '../../../assets/simpleLeftChevron.svg';
import doubleLeftChevron from '../../../assets/doubleLeftChevron.svg';
import simpleRightChevron from '../../../assets/simpleRightChevron.svg';
import doubleRightChevron from '../../../assets/doubleRightChevron.svg';
import hamburger from '../../../assets/hamburguer_logo.svg';
import searchIcon from '../../../assets/searchIcon.svg';

const defaultButton = props => <button {...props}>{props.children}</button>;

const Popup = (props) => {
  const placeholder = `${props.page} of ${props.total} pages`;
  return (
    <div className="Table_popUp Table__visiblePagesWrapper">
      <input className="Table__pageInput" type="text" placeholder={placeholder} ref={props.pageRef}/>
      <button
        className="Table__pageButton"
        onClick={props.closePopup}>
        <img src={searchIcon} alt="Set Page"/>
      </button>
    </div>
  );
};

export default class Pagination extends React.Component {
  constructor(props) {
    super();

    this.changePage = this.changePage.bind(this);

    this.state = {
      visiblePages: this.getVisiblePages(null, props.pages),
      showPopup: false,
    };
    this.pageRef = React.createRef()
  }

  componentDidUpdate(prevProps) {
    if (this.props.pages !== prevProps.pages) {
      this.setState({
        visiblePages: this.getVisiblePages(null, this.props.pages)
      });
    }
    this.changePage(this.props.page + 1);
  }

  filterPages = (visiblePages, totalPages) => {
    return visiblePages.filter(page => page <= totalPages);
  };

  getVisiblePages = (page, total) => {
    if (total < 5) {
      return this.filterPages([1, 2, 3, 4], total);
    } else {
      if (page % 4 >= 0 && page > 4 && page + 1 < total) {
        return [page - 2, page - 1, page, page + 1];
      } else if (page % 4 >= 0 && page > 4 && page + 1 >= total) {
        return [total - 3, total - 2, total - 1, total];
      } else {
        return [1, 2, 3, 4];
      }
    }
  };

  changePage(page) {
    const activePage = this.props.page + 1;

    if (page === activePage) {
      return;
    }

    const visiblePages = this.getVisiblePages(page, this.props.pages);

    this.setState({
      visiblePages: this.filterPages(visiblePages, this.props.pages)
    });

    this.props.onPageChange(page - 1);
  }

  togglePopUp() {
    this.setState({
      showPopup: !this.state.showPopup
    });
  };

  render() {
    const {PageButtonComponent = defaultButton} = this.props;
    const {visiblePages} = this.state;
    const activePage = this.props.page + 1;
    return (
      <div className="Table__pagination">
        <span className="Table__itemCount">{this.props.data.length} Results</span>
        <div className="Table__nextPageWrapper">
          <PageButtonComponent
            className="Table__paginationButton"
            onClick={() => {
              if (activePage === 1) return;
              this.changePage(1);
            }}
            disabled={activePage === 1}
          >
            <img src={doubleLeftChevron} alt="First Page"/>
          </PageButtonComponent>
          <PageButtonComponent
            className="Table__paginationButton"
            onClick={() => {
              if (activePage === 1) return;
              this.changePage(activePage - 1);
            }}
            disabled={activePage === 1}
          >
            <img src={simpleLeftChevron} alt="Previous Page"/>
          </PageButtonComponent>
        </div>
        <div className="Table__visiblePagesWrapper">
          {visiblePages.map((page) => {
            return (
              <PageButtonComponent
                key={page}
                className={
                  activePage === page
                    ? "Table__pageButton Table__pageButton--active"
                    : "Table__pageButton"
                }
                onClick={this.changePage.bind(null, page)}
              >
                {page}
              </PageButtonComponent>
            );
          })}
          <PageButtonComponent
            className={
              this.state.showPopup
                ? "Table__pageButton Table__pageButton--active"
                : "Table__pageButton"
            }
            onClick={() => this.togglePopUp()}
          >
            <img src={hamburger} alt="Select Page"/>
          </PageButtonComponent>
          {this.state.showPopup ?
            <Popup
              page={this.props.page + 1}
              pageRef={this.pageRef}
              total={this.props.pages}
              closePopup={() => {
                this.changePage(this.pageRef.current.value);
                this.togglePopUp();
              }}
            />
            : null
          }
        </div>
        <div className="Table__nextPageWrapper">
          <PageButtonComponent
            className="Table__paginationButton"
            onClick={() => {
              if (activePage === this.props.pages) return;
              this.changePage(activePage + 1);
            }}
            disabled={activePage === this.props.pages}
          >
            <img src={simpleRightChevron} alt="Next Page"/>
          </PageButtonComponent>
          <PageButtonComponent
            className="Table__paginationButton"
            onClick={() => {
              if (activePage === this.props.pages) return;
              this.changePage(this.props.pages);
            }}
            disabled={activePage === this.props.pages}
          >
            <img src={doubleRightChevron} alt="First Page"/>
          </PageButtonComponent>
        </div>
      </div>
    );
  }
}

Pagination.propTypes = {
  pages: PropTypes.number,
  page: PropTypes.number,
  PageButtonComponent: PropTypes.any,
  onPageChange: PropTypes.func,
  previousText: PropTypes.string,
  nextText: PropTypes.string
};
