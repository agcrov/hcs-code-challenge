import React from 'react';
import styles from './TaskForm.module.scss';
import {Field} from "redux-form";
import {required} from "../../../helpers/utils/validations";
import TextInput from "../TextInput/TextInput";
import SelectInput from "../SelectInput/SelectInput";
import moment from 'moment';
// import TickIcon from "../Icons/TickIcon";

const TaskStatusOptions = [
  {label: 'In progress', value: true},
  {label: 'Done', value: false},
];

//{handleSubmit, initialValues, pristine, invalid, reset }
class TaskForm extends React.Component {

  componentDidMount() {
    this.props.initialize({
      dueDate: new Date()
    });
  }

  componentWillUnmount() {
    this.props.selectTask(null);
  }

  render() {
    return (
      <div className={styles.formWrapper}>
        <h1>{`${this.props.initialValues? 'Update': 'Create'} Task`}</h1>
        <form onSubmit={this.props.handleSubmit}>
          <div className={styles.formGroup}>
            <Field
              name="description"
              label="Description"
              component={TextInput}
              validate={required}
              type="text"
            />
            <Field
              name="dueDate"
              label="Due Date"
              component={TextInput}
              validate={required}
              type="Date"
              format={(value) => moment(value).format('YYYY-MM-DD')}
            />
            {this.props.initialValues && (
              <Field
                name="isActive"
                label="State"
                component={SelectInput}
                options={TaskStatusOptions}
              />
            )}
          </div>
          <button className={styles.submitButton} disabled={this.props.pristine}>
            {/*<TickIcon pathFill={pristine || invalid ? "#8C9BA5" : "#5062FF"}/>*/}
            Save
          </button>
        </form>
      </div>
    )
  }
}

export default TaskForm;
