import {connect} from 'react-redux';
import {reduxForm} from 'redux-form';
import TaskForm from './TaskForm';
import {closeModal} from '../../../redux/UI/actions';
import {isUndefined} from 'lodash';
import {createTask, editTaskById} from "../../../redux/Tasks/actions";
import {selectTask} from "../../../redux/Tasks/actions";
import {bindActionCreators} from 'redux';


const onSubmit = (values, dispatch, props) => {
  closeModal()(dispatch);
  if(!isUndefined(props.selectedTask)){
    editTaskById(values)(dispatch);
  } else createTask(values)(dispatch);
};

const mapStateToProps = state => {
  if (state.tasks.selected_task) {
    const {_id, dueDate, description, isActive} = state.tasks.selected_task;
    return ({
      initialValues: !isUndefined(state.tasks.selected_task)
        ? {_id, dueDate, description, isActive}
        : null,
      selectedTask: state.tasks.selected_task
    });
  } else {
    return {}
  }
};

const mapDispatchToProps = dispatch => bindActionCreators({
  selectTask
}, dispatch);

const TaskFormReduxForm = reduxForm({
  form: 'taskForm',
  onSubmit,
})(TaskForm);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TaskFormReduxForm);
