import Layout from './Layout';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {logoutAction} from '../../../redux/Auth/actions';

const mapStateToProps = state => ({
  auth: state.auth
});
const mapDispatchToProps = dispatch => bindActionCreators({
  logoutAction
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Layout);
