import React from 'react';
import styles from './Layout.module.scss';
import Modal from "../Modal";

const Layout = ({auth, logoutAction, children}) => {
  return (
    <div className={styles.container}>
      <header className={styles.navBar}>
        <nav>
          <span className={styles.brandIcon}>//</span>
          <span className={styles.brandTitle}>HCS</span>
          {auth && <div className={styles.buttonsWrapper}>
            {auth.username && <span className={styles.userGreeting}>{`Hi ${auth.username}!`}</span>}
            {auth.token && <button onClick={() => logoutAction()} className={styles.logoutButton}>Logout</button>}
          </div>}
        </nav>
      </header>
      <main className={styles.content}>
        {children}
        <Modal/>
      </main>
    </div>
  );
};

export default Layout;
