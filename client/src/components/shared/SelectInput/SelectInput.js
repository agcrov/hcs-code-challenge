import React from 'react';
import Select from 'react-select';
import styles from './SelectInput.module.scss';

const customStyles = {
  indicatorSeparator: () => ({display: "none"}),
  placeholder: () => ({display: "none"}),
  option: (provided, {isSelected}) => ({
    backgroundColor: isSelected ? '#F0F3F6' : '#FFFFFF',
    fontSize: '12px',
    cursor: "pointer",
    // padding: '10px',
    width: '100%',
    minHeight: "33px",
    maxHeight: "33px",
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    padding: '0 0 0 10px'
  }),
  container: (provided) => ({
    ...provided,
    width: "100%",
    maxHeight: "33px",
  }),
  control: (provided, state) => ({
    ...provided,
    "&:hover": "none",
    minHeight: "33px",
    maxHeight: "33px",
    fontSize: "14px",
    fontWeight: 500,
    color: "black",
    boxShadow: "none",
    border: "1px solid #E5E5E5",
    borderRadius: "3px",
    cursor: "pointer",
    transition: "all 0.3s"
  }),
  dropdownIndicator: (provided, state) => ({
    margin: state.isFocused && state.selectProps.menuIsOpen ? "0 8px 6px 8px" : "6px 8px 0 8px",
    transform: state.isFocused && state.selectProps.menuIsOpen ? "scaleY(-1)" : "none",
    transition: "all 0.3s"
  }),
  menu: () => ({
    marginTop: '10px',
    maxWidth: '100%',
    boxShadow: '0 0 10px #E5E5E5',
    border: 'none',
    borderRadius: '3px',
    backgroundColor: '#FFFFFF',
  }),
};

const SelectInput = ({input, name, label, options}) => (
  <div className={styles.inputContainer}>
    <label>{label}</label>
    <Select
      {...input}
      name={name}
      options={options}
      value={input.value.value}
      defaultValue={options.filter(option => option.value === input.value)}
      onChange={value => input.onChange(value.value)}
      onBlur={() => input.onBlur(input.value.value)}
      isSearchable={false}
      styles={customStyles}
    />
  </div>
);

export default SelectInput;
