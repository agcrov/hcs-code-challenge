import React from 'react';
import styles from "./TextInput.module.scss";
import classnames from 'classnames/bind';

let cx = classnames.bind(styles);

const TextInput = ({input, label, type, placeholder, meta: {touched, error, invalid}}) => {
  const inputContainerClass = cx({
    inputContainer: true,
    invalid: touched && invalid && error,
  });
  return (
    <div className={inputContainerClass}>
      <label>{label}</label>
      <input {...input}
             type={type}
             placeholder={placeholder}
      />
      {
        touched && error && <span>{error}</span>
      }
    </div>
  );
};

export default TextInput;
