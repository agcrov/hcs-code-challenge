import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Modal from './Modal';
import {closeModal} from "../../../redux/UI/actions";

const mapStateToProps = state => ({
  isOpen: state.ui.modal.isOpen,
  type: state.ui.modal.type
});

const mapDispatchToProps = dispatch => bindActionCreators({
  closeModal,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Modal)
