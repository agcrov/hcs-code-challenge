import React from 'react';
import styles from './Modal.module.scss';
import TaskForm from '../TaskForm'

export const ModalsTypes = {
  TASK_DETAIL_FORM: 'TASK_DETAIL_FORM',
  DEFAULT: 'DEFAULT'
};
const ModalsContents = {
  TASK_DETAIL_FORM: <TaskForm/>,
  DEFAULT: null
};

const Modal = ({closeModal, type, isOpen}) => {
  let elementClasses = document.body.classList;
  isOpen? elementClasses.add(styles.activeModal): elementClasses.remove(styles.activeModal);
  return (isOpen &&
  <div className={styles.overlay} onClick={() => closeModal()}>
    <div className={styles.container} onClick={(e) => e.stopPropagation()}>
      {ModalsContents[type]}
    </div>
  </div>
)};

export default Modal;
