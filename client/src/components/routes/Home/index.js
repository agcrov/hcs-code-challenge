import {connect} from 'react-redux';
import Home from './Home';
import {bindActionCreators} from "redux";
import {fetchTasks} from "../../../redux/Tasks/actions";
import {openModal} from "../../../redux/UI/actions";

const mapStateToProps = state => ({
  tasks: state.tasks.tasks
});

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchTasks,
  openModal,
},dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
