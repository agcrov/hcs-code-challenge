import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import UpdateIcon from "../../shared/Icons/UpdateIcon";
import {parseDateCreated, parseDateString} from '../../../helpers/utils/parsers';
import {openModal} from "../../../redux/UI/actions";
import {selectTask} from "../../../redux/Tasks/actions";
import {ModalsTypes} from "../../shared/Modal/Modal";

const mapDispatchToProps = dispatch => bindActionCreators({
  openModal,
  selectTask
}, dispatch);

const EditButton = connect(null, mapDispatchToProps)(props => (
  <button onClick={() => {
    props.selectTask(props.task);
    props.openModal(ModalsTypes.TASK_DETAIL_FORM);
  }}>
    <UpdateIcon pathFill="#8C9BA5"/>
  </button>
));

const columns = [
  {
    Header: "Date Created",
    id: "createdAt",
    accessor: d => parseDateCreated(d._id),
  },
  {
    Header: "Due Date",
    id: "dueDate",
    accessor: d => parseDateString(d.dueDate),
  },
  {
    Header: "Description",
    id: "description",
    accessor: d => d.description,
  },
  {
    Header: "Status",
    id: "isActive",
    accessor: d => d.isActive ? 'In progress' : 'Done',
  },
  {
    Header: "",
    id: 'editButton',
    Cell: ({row}) => <EditButton task={row._original}/>
  },
];

export default columns;
