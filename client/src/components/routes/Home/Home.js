import React, {Component} from 'react';
import columns from "./task_table_structure";
import Table, {TableTypes} from "../../shared/Table/Table";
import styles from './Home.module.scss';
import {ModalsTypes} from '../../shared/Modal/Modal'

class Home extends Component {

  componentDidMount() {
    this.props.fetchTasks();
  }

  render() {
    return (
      <div className={styles.container}>
        <h2>Tasks</h2>
        <div className={styles.buttonsWrapper}>
          <button
            className={styles.addButton}
            onClick={() => this.props.openModal(ModalsTypes.TASK_DETAIL_FORM)}>
            + Add Task
          </button>
        </div>
        <Table
          data={this.props.tasks}
          columns={columns}
          type={TableTypes.CUSTOM_ROW_TABLE}
          itemsPerPage={6}
          paginated={true}
        />
      </div>
    );
  }
}

export default Home;
