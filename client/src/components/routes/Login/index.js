import {reduxForm} from 'redux-form';
import Login from './Login';
import {loginAction, verify} from '../../../redux/Auth/actions';
import {connect} from "react-redux";
import { bindActionCreators } from 'redux';

const onSubmit = (values, dispatch, props) => loginAction(values.username, values.password)(dispatch)
  .then(result => result.type === 'LOGIN_SUCCESS' && props.history.replace('/home'));

const mapStateToProps = state => ({
  auth: state.auth
});

const mapDispatchToProps = dispatch => bindActionCreators({
  verify
}, dispatch);

const LoginReduxForm = reduxForm({
  form: 'login',
  onSubmit,
})(Login);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginReduxForm);
