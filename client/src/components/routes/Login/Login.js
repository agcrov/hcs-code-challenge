import React, {Component} from 'react';
import styles from './Login.module.scss';
import classnames from 'classnames/bind';
import {Field} from "redux-form";
import {required} from "../../../helpers/utils/validations";
import AlertIcon from "../../shared/Icons/AlertIcon";

const cx = classnames.bind(styles);

const renderField = ({input, label, type, meta: {touched, error}}) => {
  const inputStyles = cx({
    inputField: true,
    invalid: touched && error
  });
  return (
    <div className={styles.inputContainer}>
      <input {...input}
             placeholder={label}
             type={type}
             className={inputStyles}/>
      {touched && ((error && <span className={styles.inputHelper}>{error}</span>))}
    </div>
  )
};

class Login extends Component {
  componentDidMount(){
    this.props.verify().then(() => {
      this.props.auth.authenticated && this.props.history.replace('/home')
    });
  }

  render() {
    const {handleSubmit, pristine, invalid, auth} = this.props;
    return (
      <div className={styles.container}>
        <h1 className={styles.sloganWrapper}>
          <span>Personalized</span> patient engagement. Significantly <span>better</span> outcomes.
        </h1>
        <form onSubmit={handleSubmit} className={styles.formContainer}>
          <Field
            name="username"
            type="text"
            component={renderField}
            label="Username"
            validate={[required]}
          />
          <Field
            name="password"
            type="password"
            component={renderField}
            label="Password"
            validate={[required]}
          />
          <div>
            <button type="submit" disabled={pristine && invalid} className={styles.loginButton}>
              Log in
            </button>
          </div>
        </form>
        {auth.error && <div className={styles.loginError}><span>{auth.error}</span> <AlertIcon pathFill="#F4C76F"/></div>}
      </div>
    );
  }
}

export default Login;
