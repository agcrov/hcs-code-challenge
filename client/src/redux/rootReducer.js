import { combineReducers } from 'redux';
import {reducer as formReducer} from 'redux-form';
import authReducer from './Auth/reducer';
import taskReducer from './Tasks/reducer';
import uiReducer from './UI/reducer';

export default combineReducers({
  form: formReducer,
  auth: authReducer,
  tasks: taskReducer,
  ui: uiReducer,
});
