import {TaskActionsTypes as types} from "./actions";
import {isEmpty, isNull} from "lodash";
import {updateObjectInArray} from "../../helpers/utils/redux";


const initialState = {
  tasks: [],
  selected_task: null,
  error: null,
  isPending: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case  types.SELECT_TASK:
      return {
        ...state,
        selected_task: action.payload,
      };
    case  types.FETCH_TASKS_SUCCESS:
      return {
        ...state,
        tasks: action.payload,
        isPending: false,
      };
    case types.FETCH_TASKS_STARTED:
      return {
        ...state,
        isPending: true,
      };
    case types.FETCH_TASKS_FAILURE:
      return {
        ...state,
        error: action.payload,
        isPending: false,
      };
    case types.CREATE_TASK_STARTED:
      return {
        ...state,
        isPending: true
      };
    case types.CREATE_TASK_SUCCESS:
      return {
        ...state,
        tasks: [...state.tasks, action.payload],
        isPending: false
      };
    case types.CREATE_TASK_FAILURE:
      return {
        ...state,
        error: action.payload,
        isPending: false
      };
    case types.EDIT_TASK_BYID_STARTED:
      return {
        ...state,
        selected_task: null,
        isPending: true
      };
    case types.EDIT_TASK_BYID_SUCCESS:
      const itemIndex = !isEmpty(state.tasks)
        ? state.tasks.findIndex(task => task._id === action.payload._id)
        : null;
      return {
        ...state,
        tasks: isNull(itemIndex)
          ? state.tasks
          : updateObjectInArray(state.tasks, itemIndex, action.payload),
        isPending: false
      };
    case types.EDIT_TASK_BYID_FAILURE:
      return {
        ...state,
        error: action.payload,
        isPending: false
      };
    default:
      return state;
  }
}
