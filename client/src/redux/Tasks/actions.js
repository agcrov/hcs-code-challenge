import {getTasks, postTask, patchTask } from "../../helpers/api/tasks";

export const TaskActionsTypes = {
  FETCH_TASKS_STARTED: 'FETCH_TASKS_STARTED',
  FETCH_TASKS_SUCCESS: 'FETCH_TASKS_SUCCESS',
  FETCH_TASKS_FAILURE: 'FETCH_TASKS_FAILURE',
  EDIT_TASK_BYID_STARTED: 'EDIT_TASK_BYID_STARTED',
  EDIT_TASK_BYID_SUCCESS: 'EDIT_TASK_BYID_SUCCESS',
  EDIT_TASK_BYID_FAILURE: 'EDIT_TASK_BYID_FAILURE',
  CREATE_TASK_STARTED: 'CREATE_TASK_STARTED',
  CREATE_TASK_SUCCESS: 'CREATE_TASK_SUCCESS',
  CREATE_TASK_FAILURE: 'CREATE_TASK_FAILURE',
  SELECT_TASK: 'SELECT_TASK',
};

export const selectTask = (task) => dispatch => {
  dispatch({
    type: TaskActionsTypes.SELECT_TASK,
    payload: task
  })
};

export const fetchTasks = () => dispatch => {
  dispatch({
    type: TaskActionsTypes.FETCH_TASKS_STARTED,
  });
  getTasks().then(response => {
    dispatch({
      type: TaskActionsTypes.FETCH_TASKS_SUCCESS,
      payload: response.data
    });
  })
    .catch(error => dispatch({
      type: TaskActionsTypes.FETCH_TASKS_FAILURE,
      error: error && error.response ? error.response.data.error : error.message,
    }));
};

export const editTaskById = ({_id, ...data}) => dispatch => {
  dispatch({
    type: TaskActionsTypes.EDIT_TASK_BYID_STARTED,
  });
  return patchTask(_id, data)
    .then(response => dispatch({
      type: TaskActionsTypes.EDIT_TASK_BYID_SUCCESS,
      payload: response.data,
    }))
    .catch(error => dispatch({
      type: TaskActionsTypes.EDIT_TASK_BYID_FAILURE,
      payload: error && error.response ? error.response.data.error : error.message,
    }));
};

export const createTask = (data) => dispatch => {
  dispatch({
    type: TaskActionsTypes.CREATE_TASK_STARTED,
  });
  return postTask(data)
    .then(response => dispatch({
      type: TaskActionsTypes.CREATE_TASK_SUCCESS,
      payload: response.data,
    }))
    .catch(error => dispatch({
      type: TaskActionsTypes.CREATE_TASK_FAILURE,
      payload: error && error.response ? error.response.data.error : error.message,
    }));
};
