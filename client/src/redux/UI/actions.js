export const UIActions = {
  OPEN_MODAL: 'OPEN_MODAL',
  CLOSE_MODAL: 'CLOSE_MODAL',
};

export const openModal = (type) => dispatch => dispatch({
  type: UIActions.OPEN_MODAL,
  payload: type
});

export const closeModal = () => dispatch => dispatch({
  type: UIActions.CLOSE_MODAL,
});
