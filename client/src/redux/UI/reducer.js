import {UIActions} from './actions';

const initialState = {
  modal: {isOpen: false, type: null},
  error: null,
};


export default function (state = initialState, action) {
  switch (action.type) {
    case UIActions.OPEN_MODAL:
      return {
        ...state,
        modal: {isOpen: true, type: action.payload}
      };

    case UIActions.CLOSE_MODAL:
      return {
        ...state,
        modal: {isOpen: false, type: null}
      };

    default:
      return state;
  }
}
