import {AuthActions as types} from './actions'
import Cookies from 'cookies-js';
import jwt from 'jsonwebtoken';

const hasCookies = Cookies.get('JWTPayload') && Cookies.get('JWTSignature');
const payload = jwt.decode(`${Cookies.get('JWTPayload')}.${Cookies.get('JWTSignature')}`);

const initialState = {
  token: hasCookies
    ? `${Cookies.get('JWTPayload')}.${Cookies.get('JWTSignature')}`
    : null,
  roles: hasCookies? payload? payload.roles : null : null,
  isPending: false,
  authenticated: undefined,
  error: null
};

export default function (state = initialState, action) {
  switch (action.type) {
    case types.LOGIN_STARTED:
      return {
        ...state,
        isPending: true,
      };
    case types.LOGIN_SUCCESS:
      return {
        ...state,
        token: action.payload.token,
        username: action.payload.username,
        roles: jwt.decode(action.payload.token).roles,
        authenticated: true,
        isPending: false,
        error: null,
      };
    case types.LOGIN_FAILURE:
      return {
        ...state,
        token: null,
        username: null,
        roles: null,
        error: action.payload,
        isPending: false,
      };
    case types.LOGOUT_STARTED:
      return {
        ...state,
        isPending: true,
      };
    case types.LOGOUT_SUCCESS:
      return {
        ...state,
        token: null,
        username: null,
        roles: null,
        isPending: false,
        authenticated: false,
        error: null,
      };
    case types.LOGOUT_FAILURE:
      return {
        ...state,
        error: action.payload,
        isPending: false,
      };
    case types.VERIFY_TOKEN_STARTED:
      return {
        ...state,
        isPending: true
      };
    case types.VERIFY_TOKEN_SUCESS:
      return {
        ...state,
        isPending: false,
        authenticated: true
      };
    case types.VERIFY_TOKEN_FAILURE:
      return {
        ...state,
        authenticated: false,
        token: null,
        isPending: false
      };
    default:
      return state;
  }
}
