import {login, logout, verifyToken} from "../../helpers/api/auth";
import Cookies from 'cookies-js';

export const AuthActions = {
  LOGIN_STARTED: 'LOGIN_STARTED',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  LOGIN_FAILURE: 'LOGIN_FAILURE',

  LOGOUT_STARTED: 'LOGOUT_STARTED',
  LOGOUT_SUCCESS: 'LOGOUT_SUCCESS',
  LOGOUT_FAILURE: 'LOGOUT_FAILURE',

  VERIFY_TOKEN_STARTED: 'VERIFY_TOKEN_STARTED',
  VERIFY_TOKEN_SUCESS: 'VERIFY_TOKEN_SUCESS',
  VERIFY_TOKEN_FAILURE: 'VERIFY_TOKEN_FAILURE'
};

export const loginAction = (username, password) => dispatch => {
  dispatch({
    type: AuthActions.LOGIN_STARTED,
  });
  return login(username, password)
    .then(response => {
      const [header, payload, signature] = response.data.token.split('.');
      Cookies.set('JWTPayload', `${header}.${payload}`, {
        secure: false,
        domain: window.location.hostname,
        expires: 3600 * 1000, // ONE HOUR
      });
      Cookies.set('JWTSignature', signature, {
        secure: false,
        domain: window.location.hostname,
        httpOnly: true,
      });
      return dispatch({
        type: AuthActions.LOGIN_SUCCESS,
        payload: response.data
      })
    })
    .catch(error => dispatch({
      type: AuthActions.LOGIN_FAILURE,
      payload: error && error.response ? error.response.data.error : error.message,
    }));
};

export const logoutAction = () => dispatch => {
  dispatch({
    type: AuthActions.LOGOUT_STARTED,
  });
  return logout()
    .then(() => {
      Cookies.expire('JWTPayload');
      Cookies.expire('JWTSignature');
      return dispatch({
        type: AuthActions.LOGOUT_SUCCESS,
      })
    })
    .catch(error => {
      dispatch({
      type: AuthActions.LOGOUT_FAILURE,
      payload: error.response.data,
    })
    });
};

export const verify = () => dispatch => {
  dispatch({
    type: AuthActions.VERIFY_TOKEN_STARTED,
  })
  return verifyToken().then(() => {
    return dispatch({
      type: AuthActions.VERIFY_TOKEN_SUCESS
    })
  })
  .catch(error => {
    dispatch({
      type: AuthActions.VERIFY_TOKEN_FAILURE
    })
  });
}
