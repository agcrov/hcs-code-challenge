import moment from 'moment';

export const parseDateString = (date) => moment(new Date(date)).format('MMM D, h:mm');

export const parseDateCreated = (id) => {
  let timestamp = id.toString().substring(0, 8);
  return moment(parseInt(timestamp, 16) * 1000).format('MMM D, h:mm');
};
