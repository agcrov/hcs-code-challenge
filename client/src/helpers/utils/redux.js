const removeItem = (array, action) => {
  return [...array.slice(0, action.index), ...array.slice(action.index + 1)]
};

const updateObjectInArray = (array, itemIndex, newItem) => {
  return array.map((item, index) => {
    if (index !== itemIndex) {
      // This isn't the item we care about - keep it as-is
      return item
    }
    // Otherwise, this is the one we want - return an updated value
    return {
      ...item,
      ...newItem
    }
  })
};

export {
  removeItem,
  updateObjectInArray
}
