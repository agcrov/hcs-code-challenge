import moment from "moment";

export const date = value => value ? moment(new Date(value), 'L').isValid() ? undefined : 'Invalid date' : undefined;
export const required = value => value ? undefined : 'Required';
