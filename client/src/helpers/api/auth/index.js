import axios from 'axios';
import {post,get} from '../index';
const baseUrl = `http://localhost:8000/auth`;

const login = (username, password) => axios.post(`${baseUrl}/login`, {username, password});
const logout = () => post('/auth/logout');
const verifyToken = () => get('/auth/verify');

export {
  login,
  logout,
  verifyToken
}
