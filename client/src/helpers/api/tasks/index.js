import {get, post, patch} from '../index';

const getTasks = () => get('/tasks/');
const postTask = (data) => post('/tasks/', data);
const patchTask = (id, data) => patch(`/tasks/${id}`, data);

export {
  getTasks,
  postTask,
  patchTask,
}
