import axios from 'axios';
import Cookies from 'cookies-js';
const apiDomain = 'http://localhost:8000';

const options = () => ({
  headers: {"Authorization": `${Cookies.get('JWTPayload')}.${Cookies.get('JWTSignature')}`}
});

export const get = endpoint => axios.get(`${apiDomain}${endpoint}`, options());
export const post = (endpoint, data = {}) => axios.post(`${apiDomain}${endpoint}`, data, options());
export const put = (endpoint, data = {}) => axios.put(`${apiDomain}${endpoint}`, data, options());
export const patch = (endpoint, data = {}) => axios.patch(`${apiDomain}${endpoint}`, data, options());
export const del = (endpoint) => axios.delete(`${apiDomain}${endpoint}`, options());
