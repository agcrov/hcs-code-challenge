import React from 'react';
import Layout from "./components/shared/Layout";
import {Provider} from 'react-redux';
import store from './redux/store';
import {BrowserRouter as Router, Redirect, Route, Switch} from 'react-router-dom';
import PrivateRoute from "./components/shared/PrivateRoute";
import Login from "./components/routes/Login";
import Home from "./components/routes/Home";

function App() {
  return (
    <Provider store={store}>
      <Layout>
        <Router>
          <Switch>
            <Route exact path="/login" component={Login}/>
            <PrivateRoute path="/home" component={Home}/>
            <Redirect to="/home"/>
          </Switch>
        </Router>
      </Layout>
    </Provider>
  );
}

export default App;
