import express from 'express';
import dotEnv from 'dotenv';
import cors from 'cors';
import helmet from 'helmet';
import boom from 'express-boom';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import mongoose from 'mongoose';
import appRouter from './routes';

dotEnv.config();

const app = express();

mongoose.connect(process.env.DB_URI, {useNewUrlParser: true, useCreateIndex: true});
// CONNECTION HANDLERS
mongoose.connection.on('connected', () => {
  console.log(`Connected to database ${process.env.DB_URI}`);
});
// Retry connection
const connectWithRetry = () => {
  console.log('MongoDB connection with retry');
  return mongoose.connect(process.env.DB_URI, {useNewUrlParser: true, useCreateIndex: true});
};
// Exit application on error
mongoose.connection.on('error', (err) => {
  console.log(`MongoDB connection error: ${err}`);
  setTimeout(connectWithRetry, 5000);
  // process.exit(-1)
});

app.use(cors());
app.use(helmet());
app.use(cookieParser());
app.use(boom());
app.use(bodyParser.json());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use('/', appRouter);

export default app;
