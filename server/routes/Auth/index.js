import express from 'express';
import AuthController from './controller';
import validateToken from '../../middlewares/validateToken';


const router = express.Router();

router.post('/login', AuthController.login);
router.post('/logout', validateToken, AuthController.logout);
router.post('/register', AuthController.register);
router.get('/verify', validateToken, AuthController.verify);

export default router;
