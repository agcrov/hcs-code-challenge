import User from '../../models/user';
import {isUndefined} from 'lodash';
import {createToken, getTokenPayload} from "../../helpers/Auth/tokenHelper";

class AuthController {
  login(req, res) {
    try {
      if (!isUndefined(req.body.username) && !isUndefined(req.body.password)) {
        User.findOne({username: req.body.username}).then((user) => {
          if (!isUndefined(user)) {
            user.comparePassword(req.body.password, (err, isMatch) => {
              if (err) throw err;
              if (isMatch) {
                user.token = createToken(user._id, user.roles);
                user.save()
                  .then(() => res.status(200).json({username: user.username, token: user.token}))
                  .catch(err => res.boom.badImplementation(err.message));
              } else res.boom.unauthorized('invalid password');
            });
          } else res.boom.notFound('User not found.');
        }).catch((err) => res.boom.badImplementation(err.message));
      } else res.boom.badRequest('Invalid request data.');
    } catch (e) {
      res.boom.badImplementation('Unexpected error.');
    }
  };

  logout(req, res) {
    try {
      console.log(req.headers)
      if (!isUndefined(req.headers.authorization)) {
      const payload = getTokenPayload(req.headers.authorization);
        User.findById(payload.sub)
          .then(user => {
            if (!isUndefined(user)) {
              user.token = undefined;
              user.save()
                .then(() => res.sendStatus(200))
                .catch(err => res.boom.badRequest(err.message));
            } else res.boom.notFound('User not found.');
          })
          .catch(err => res.boom.badRequest(err.message));
      } else res.boom.badRequest('Invalid request data.');
    } catch (e) {
      res.boom.badImplementation('Unexpected error.');
    }
  };

  register(req, res) {
    try {
      if (!isUndefined(req.body.username) && !isUndefined(req.body.password)) {
        const user = new User({username: req.body.username, password: req.body.password});
        user.save()
          .then(() => res.status(200).send())
          .catch(err => res.boom.badRequest(err.message));
      } else res.boom.badRequest('Invalid request data.');
    } catch (e) {
      res.boom.badImplementation('Unexpected error.');
    }
  };

  verify(req, res) {
    res.status(200).json({
      authenticated: 'Ok'
    })
  }
}

export default new AuthController();
