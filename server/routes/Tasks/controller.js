import Task from '../../models/task';
import {isUndefined} from 'lodash';
import {getTokenPayload} from "../../helpers/Auth/tokenHelper";

class TaskController {
  getUserTasks({headers: {authorization}}, res) {
    try {
      if (!isUndefined(authorization)) {
        const payload = getTokenPayload(authorization);
        Task.find({author: payload.sub})
          .then(tasks => {
            res.status(200).json(tasks);
          })
          .catch(err => res.boom.badRequest(err.message));
      } else res.boom.badRequest('Invalid request data.');
    } catch (e) {
      res.boom.badImplementation('Unexpected error.');
    }
  };

  addTask({body: {description, dueDate}, headers: {authorization}}, res) {
    try {
      if (!isUndefined(authorization) && !isUndefined(description) && !isUndefined(dueDate)) {
        const payload = getTokenPayload(authorization);
        const task = new Task({author: payload.sub, description: description, dueDate: dueDate});
        task.save()
          .then(() => res.status(200).json(task))
          .catch(err => res.boom.badRequest(err.message));
      } else res.boom.badRequest('Invalid request data.');
    } catch (e) {
      res.boom.badImplementation('Unexpected error.');
    }
  }

  updateTask({ headers: {authorization}, params: {id}, body}, res) {
    try {
      if (!isUndefined(authorization) && !isUndefined(id) && !isUndefined(body)) {
        const payload = getTokenPayload(authorization);
        Task.updateOne({_id: id, author: payload.sub}, body)
          .then(task => {
            if (!isUndefined(task)) {
              res.status(200).json(task);
            } else res.boom.notFound('Task not found.');
          })
          .catch(err => res.boom.badRequest(err.message));
      } else res.boom.badRequest('Invalid request data.');
    } catch (e) {
      res.boom.badImplementation('Unexpected error.');
    }
  };
}

export default new TaskController();
