import express from 'express';
import TaskController from "./controller";
import validateToken from '../../middlewares/validateToken';

const router = express.Router();

router.post('/',validateToken, TaskController.addTask);
router.get('/',validateToken, TaskController.getUserTasks);
router.patch('/:id',validateToken, TaskController.updateTask);


export default router;
