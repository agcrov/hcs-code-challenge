import express from 'express';
import authRouter from './Auth';
import tasksRouter from './Tasks';

const router = express.Router();

router.use('/auth', authRouter);
router.use('/tasks', tasksRouter);

export default router;
