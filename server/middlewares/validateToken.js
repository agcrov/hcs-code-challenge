import jwt from 'jsonwebtoken';
import User from '../models/user';

const validateToken = (req, res, next) => {
  try {
    const token = req.headers.authorization;
    jwt.verify(token, process.env.SECRET, (err, decodedToken) => {
      if(decodedToken) {
        User.findById(decodedToken.sub).then(user => {
          if(user.token === token) {
            next();
          } else {
            res.boom.unauthorized('Invalid token');
          }
        }).catch(e => {
          res.boom.unauthorized('Invalid token');
        });
      } else {
        res.boom.unauthorized('Invalid token');
      }
    });
  } catch (e) {
    res.boom.badImplementation('Unexpected error.');
  }
};

export default validateToken;