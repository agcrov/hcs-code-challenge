import jwt from 'jsonwebtoken';
import moment from 'moment';

export const createToken = (id, roles) => {
  const utcNow = moment().utc(false);
  const payload = {
    sub: id,
    roles: roles,
    iat: utcNow.unix(),
    exp: utcNow.add(1, 'days').unix(),
  };
  return jwt.sign(payload, process.env.SECRET);
};

export const getTokenPayload = token => jwt.decode(token);
