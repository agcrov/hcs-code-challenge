import mongoose, {Schema, model} from 'mongoose';
import bcrypt from 'bcrypt';

const userSchema = new Schema({
  username: {type: String, unique: true, required: true, dropDups: true},
  password: {type: String, required: true},
  token: {type: String},
  roles: {type: [String], enum: ['user', 'admin'], required: true, default: ['user']}
}, {timestamp: true, strict: true});

userSchema.pre('save', function (next) {
  const user = this;
  if (!user.isModified('password')) {
    return next();
  }
  bcrypt.hash(user.password, 10).then((hashedPassword) => {
    user.password = hashedPassword;
    next();
  });
}, function (err) {
  next(err)
});

userSchema.method('comparePassword', function (candidatePassword, next) {
  bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
    if (err) return next(err);
    next(null, isMatch)
  })
});

export default mongoose.model('User', userSchema)
