import mongoose, { Schema } from 'mongoose'

const taskSchema = new Schema({
  author: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
  description: { type: String, required: true },
  dueDate: { type: Date, required: true },
  isActive: { type: Boolean, default: true },
},{timestamp: true, strict: true });

export default mongoose.model('Task', taskSchema)
